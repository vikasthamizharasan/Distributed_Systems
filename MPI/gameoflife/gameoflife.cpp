/*
 * Copyright ( c ) 2017 Vikas T
 */

#include <mpi.h>
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <unistd.h>
#include <cstring>

#define NSIZE 3
#define toDigit (c)(c - '0')

using namespace std;

int** allocate_2d(int size) {
  int* data = (int*)malloc(size * size * sizeof(int));
  int** array = (int**)malloc(size * sizeof(int*));
  for (int i = 0; i < size; i++) array[i] = &(data[size * i]);
  return array;
}

int matrixSize(char* filename) {
  string line;
  ifstream f(filename);
  while (getline(f, line))
    if (line.length() > 0) return line.length();
}

int** getNeighbours(int** matrix, int row, int col, int size) {
  int** neighbours = allocate_2d(NSIZE);
  for (int i = -1; i < 2; i++)
    for (int j = -1; j < 2; j++) {
      if ((i + row) < 0 || (i + row >= size))
        neighbours[i + 1][j + 1] = -1;
      else if ((j + col) < 0 || (j + col >= size))
        neighbours[i + 1][j + 1] = -1;
      else
        neighbours[i + 1][j + 1] = matrix[i + row][j + col];
    }

  return neighbours;
}

void printMatrix(int** matrix, int size, int t) {
  unsigned int microseconds = 200000;
  cout << "time = " << t << endl;
  for (int i = 0; i < size; i++) {
    for (int j = 0; j < size; j++) {
      if (matrix[i][j] == 1)
        cout << "◼"
             << " ";
      else
        cout << "◻"
             << " ";
    }
    cout << endl;
  }
  usleep(microseconds);
  cout << "\033[2J\033[1;1H";
}

int main(int argc, char** argv) {
  char* filename;
  int timesteps;

  MPI_Init(NULL, NULL);

  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  if (world_rank == 0) {
    filename = argv[1];
    ifstream file(filename);
    if (!file.is_open()) return -1;

    timesteps = atoi(argv[2]);
    int size = matrixSize(filename);
    int** matrix = new int*[size];

    // Read matrix from File
    string line;
    int x = 0;
    while (getline(file, line)) {
      matrix[x] = new int[size];
      for (int y = 0; y < size; y++) matrix[x][y] = toDigit(line[y]);
      x++;
    }

    // Partition matrix for each process
    int remain = (size * size) % (world_size - 1);
    int* matrix_parts = new int[world_size - 1];
    int* copy_parts = new int[world_size - 1];

    for (int i = 0; i < world_size - 1; i++) {
      matrix_parts[i] = (size * size) / (world_size - 1);

      if (remain > 0) {
        matrix_parts[i]++;
        remain--;
      }

      int count = matrix_parts[i] * timesteps;
      MPI_Send(&count, 1, MPI_INT, i + 1, 0, MPI_COMM_WORLD);
    }

    int t = 0;
    while (t++ < timesteps) {
      copy(matrix_parts, matrix_parts + world_size - 1, copy_parts);

      // Read matrix and send neighbours to each process
      int current_rank = 1;
      int index = 0;
      for (int i = 0; i < size; i++) {
        for (int j = 0; j < size; j++) {
          if (copy_parts[current_rank - 1] == 0) current_rank++;

          int** neighbours;
          neighbours = getNeighbours(matrix, i, j, size);
          MPI_Send(&index, 1, MPI_INT, current_rank, 0, MPI_COMM_WORLD);
          MPI_Send(&(neighbours[0][0]), NSIZE * NSIZE, MPI_INT, current_rank, 0,
                   MPI_COMM_WORLD);
          copy_parts[current_rank - 1]--;
          index++;
        }
      }

      // Receive cell value and update matrix
      for (int i = 1; i < world_size; i++) {
        for (int j = 0; j < matrix_parts[i - 1]; j++) {
          int update;
          int index;

          MPI_Recv(&index, 1, MPI_INT, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
          MPI_Recv(&update, 1, MPI_INT, i, 0, MPI_COMM_WORLD,
                   MPI_STATUS_IGNORE);
          matrix[index / size][index % size] = update;
        }
      }

      printMatrix(matrix, size, t);
    }

    for (int i = 0; i < size; ++i) delete[] matrix[i];
    delete[] matrix;
  }

  if (world_rank != 0) {
    int count;
    MPI_Recv(&count, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

    while (count-- > 0) {
      int index;
      int** neighbours = allocate_2d(NSIZE);

      MPI_Recv(&index, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      MPI_Recv(&(neighbours[0][0]), NSIZE * NSIZE, MPI_INT, 0, 0,
               MPI_COMM_WORLD, MPI_STATUS_IGNORE);

      int sum = 0;
      int current_cell = neighbours[1][1];

      for (int i = 0; i < NSIZE; i++) {
        for (int j = 0; j < NSIZE; j++) {
          if (neighbours[i][j] != -1) sum += neighbours[i][j];
        }
      }

      sum -= current_cell;
      if ((current_cell == 1 && (sum == 2 || sum == 3)) ||
          (current_cell == 0 && sum == 3)) {
        sum = 1;
        MPI_Send(&index, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
        MPI_Send(&sum, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
      } else {
        sum = 0;
        MPI_Send(&index, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
        MPI_Send(&sum, 1, MPI_INT, 0, 0, MPI_COMM_WORLD);
      }
    }
  }

  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
}
