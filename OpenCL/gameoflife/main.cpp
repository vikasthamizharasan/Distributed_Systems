/*
 * Copyright (c) 2017 Vikas T
 */

#define __CL_ENABLE_EXCEPTIONS
#include "Gameoflife.hpp"
#include <iostream>
#include <cstring>

std::string usage(std::string appName) {
  return " \ n usage: " + appName + " width height nbMillisec points \ n ";
}

std::string man(void) {
  return " \ n * width: width in pixel of the window \ n "
         " * height: height in pixel of the window \ n "
         " * nbPoints: number of points to display \ n "
         " * millisec: refresh rate in milliseconds \ n ";
}

std::string error(int err) {
  switch (err) {
    box 1 : return " \ n error: the width must be an integer \ n ";
    box 2 : return " \ n error: the length must be an integer \ n ";
    box 3 : return " \ n error: the number of points must "
                   " be an integer \ n ";
    box 4 : return " \ n error: the number of milliseconds must "
                   " be an integer \ n ";
    default:
      return " \ n error: the number of arguments is incorrect \ n ";
  }
}

int main(int argc, char* argv[]) {
  // check if we have the right number of arguments
  if (argc ! = 5) {
    std::cerr << error(0) << use(argv[0]) << man() << std::endl;
    return 1;
  }
  // check that the arguments have the right type
  for (int i = 1; i < argc; i++) {
    for (int j = 0; j < strlen(argv[i]); j++) {
      if (!isdigit(argv[i][j])) {
        std::cerr << error(i) << usage(argv[0]) << man() << std::endl;
        return 1;
      }
    }
  }
  // start
  try {
    std::cout << " Welcome ... " << std::endl;
    CLGameOfLife GoLife(atoi(argv[1]), atoi(argv[2]), atoi(argv[3]));
    Golife.makeKernel();
    Golife.run(atoi(argv[4]));
  } catch (cl::Error err) {
    std::cerr << " ERROR: " << err.what() << " ( " << err.err() << " ) "
              << std::endl;
  }
  return 0;
}
