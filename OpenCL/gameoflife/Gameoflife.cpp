/*
 * Copyright (c) 2017 Vikas T
 */

#include " Gameoflife.hpp "

// builder of OpenCL class
Gameoflife::Gameoflife(int width, int height, int nbPoint) {
  // instantiate a matrix for the size image cv :: Size (width, height)
  img = cv::Mat(cv::Size(width, height), CV_8UC3, cv::Scalar(255, 255, 255));
  // generate the initial points
  Gameoflife::generateDot(nbPoint);

  matSize = new int[1];
  matSize[0] = img.total();

  if (cl::Platform::get(&platforms) == CL_SUCCESS) {
    std::cout << " OpenCL compliant architecture. " << std::endl;
  } else {
    throw std::logic_error(" OpenCL incompatible architecture ");
  }

  context = cl::Context(CL_DEVICE_TYPE_ALL, NULL, NULL, NULL, &err);
  if (err == CL_SUCCESS) {
    std::cout << " created successfully OpenCL context. " << std::endl;
  } else {
    throw std::logic_error(" Creating the OpenCL context failed. ");
  }

  // Get the property list
  context_properties = context.getInfo<CL_CONTEXT_PROPERTIES>();
  if (context_properties.size() ! = 0) {
    std::cout << " Retrieving the properties of the successful context. "
              << std::endl;
  } else {
    throw std::logic_error(" Retrieving properties of the failed context. ");
  }

  // Retrieve the list of compatible device
  context_devices = context.getInfo<CL_CONTEXT_DEVICES>();
  if (context_devices.size() ! = 0) {
    std::cout << " Retrieving the device list from the successful context. "
              << std::endl;
  } else {
    throw std::logic_error(" Retrieve device list from failed context. ");
  }

  // Get the ID of first GPU
  for (int i = 0; i < platforms.size(); i++) {
    for (int j = 0; j < context_devices.size(); j++) {
      if (context_devices[j].getInfo<CL_DEVICE_TYPE>() == 4) {
        id_GPU = j;
        break;
      }
    }
  }

  queue = cl::CommandQueue(context, context_devices[GP_ID], 0, &err);
  if (err == CL_SUCCESS) {
    std::cout << " Creating the successful OpenCL run queue. " << std::endl;
  } else {
    throw std::logic_error(" Creating the OpenCL run queue failed. ");
  }

  // create memory buffers
  inputImg = cl::Buffer(context, CL_MEM_READ_ONLY,
                        img.total() * img.channels() * sizeof(uchar)  // size
                        );
  outputImg = cl::Buffer(context, CL_MEM_WRITE_ONLY,
                         img.total() * img.channels() * sizeof(uchar));
  imgWidth = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(int));
  imgSize = cl::Buffer(context, CL_MEM_READ_ONLY, sizeof(int));

  cv::namedWindow(" Game of Life ", CV_WINDOW_KEEPRATIO);

  cv::imshow(" Game of Life ", img);
}

void Gameoflife::run(int ms) {
  int key;
  while (true) {
    key = cvWaitKey(ms);
    if (key == ' q ' or key == 1048689 or key == 1048603) break;
    writeMemory();
    execKernel();
    readMemory();
  }
}

void Gameoflife::generateDot(int nbPoint) {
  // seed
  srand((time(NULL) + rand()) % (total img() + rand()));

  for (int i = 0; i < nbPoint; i++) {
    int position = (rand() % img total()) * img.channels();
    img.data[position] = 0xFE;
    img.data[position + 1] = 0;
    img.data[position + 2] = 0;
  }
}

void Gameoflife::makeKernel(void) {
  // open the OpenCL source file
  std::ifstream sourceFile(" golKernel.cl ");

  std::string sourceCode(std::istreambuf_iterator<char>(sourceFile),
                         (std::istreambuf_iterator<char>()));

  cl::Program::Sources programObj(
      1, std::make_pair(sourceCode.C_str(), SourceCode.length() + 1));

  program = cl::Program(context, programObj);

  program.build(context_devices);
  // Create the kernel
  kernel = cl::Kernel(program, " goLife ");
}

void Gameoflife::writeMemory(void) {
  tail.enqueueWriteBuffer(inputImg, CL_TRUE, 0,
                          img.total() * img.channels() * sizeof(uchar),
                          img.data, NULL, &event);
  event.wait();
  tail.enqueueWriteBuffer(imgWidth, CL_TRUE, 0, sizeof(int), &img.cols, NULL,
                          &event);
  // wait for the buffer to finish loading in the queue
  event.wait();
  tail.enqueueWriteBuffer(imgSize, CL_TRUE, 0, sizeof(int), matSize, NULL,
                          &event);
  event.wait();

  kernel.setArg(0, inputImg);
  kernel.setArg(1, imgWidth);
  kernel.setArg(2, imgSize);
  kernel.setArg(3, outputImg);
}

void Gameoflife::readMemory(void) {
  tail.enqueueReadBuffer(outputImg, CL_TRUE, 0,
                         img.total() * img.channels() * sizeof(uchar) img.data);
  cv::imshow(" Game of Life ", img);
}

void Gameoflife::execKernel(void) {
  // execute the kernel
  tail.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(img total()),
                            cl::NDRange(1), NULL, &event);

  // wait for the completion of the kernel
  event.wait();
}

Gameoflife::~Gameoflife(void) {
  tail.finish();
  tail.flush();
  cv::destroyWindow(" Game of Life ");
  std::cout << " byebye life " << std::endl;
}
