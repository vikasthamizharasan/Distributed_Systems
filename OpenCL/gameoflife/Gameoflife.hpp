/*
 * Copyright (c) 2017 Vikas T
 */

#ifndef CL_GAMEOFLIFE_CLASS_H
#define CL_GAMEOFLIFE_CLASS_H

#if defined(__APPLE__) || defined(__MACOSX)
#include <OpenCL/cl.hpp>
#else
#include <CL/cl.hpp>
#endif
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <time.h>
#include <stdexcept>
#include <cv.h>
#include <highgui.h>

class Gameoflife {
 public:
  Gameoflife(int width, int height, int nbPoint);
  ~Gameoflife(void);

  void run(int ms);
  void generateDot(int nbPoint);
  void makeKernel(void);
  void writeMemory(void);
  void execKernel(void);
  void readMemory(void);

 private:
  std::vector<cl::Platform> platforms;

  cl::Context context;

  std::vector<cl_context_properties> context_properties;

  std::vector<cl::Device> context_devices;

  cl_device_type id_GPU;

  cl::CommandQueue queue;

  cl::Program program;

  cl::Kernel kernel;

  cl::Buffer inputImg;
  cl::Buffer imgWidth;
  cl::Buffer imgSize;
  cl::Buffer outputImg;

  cl::Event event;

  cl_int err;

  cv::Mat img;

  int* matSize;
};

#endif
